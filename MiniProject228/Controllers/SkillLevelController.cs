﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewModel;

namespace MiniProject228.Controllers
{
    public class SkillLevelController : Controller
    {
        // GET: Keahlian
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult List()
        {
            return PartialView("_List", SkillLevelRepo.All());
        }
        public ActionResult Create()
        {
            return PartialView("_Create", new SkillViewModel());
        }

        [HttpPost]
        public ActionResult Create(SkillViewModel model)
        {
            ResponseResult result = SkillLevelRepo.Update(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Edit(int id)
        {
            // id => Category Id
            return PartialView("_Edit", SkillLevelRepo.byId(id));
        }

        [HttpPost]
        public ActionResult Edit(SkillViewModel model)
        {
            ResponseResult result = SkillLevelRepo.Update(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Delete(int id)
        {
            // id => Category Id
            return PartialView("_Delete", SkillLevelRepo.byId(id));
        }

        [HttpPost]
        public ActionResult Delete(SkillViewModel model)
        {
            ResponseResult result = SkillLevelRepo.Delete(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }
    }
}