﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewModel;

namespace MiniProject228.Controllers
{
    public class EducationLevelController : Controller
    {
        // GET: EducationLevel
        public ActionResult Index()
        {
            return View();
        }
    public ActionResult List()
        {
            return PartialView("_List", EducationLevelRepo.All());
        }
        public ActionResult Create()
        {
            return PartialView("_Create", new EducationLevelViewModel());
        }

        [HttpPost]
        public ActionResult Create(EducationLevelViewModel model)
        {
            ResponseResult result = EducationLevelRepo.Update(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Edit(int id)
        {
            // id => Category Id
            return PartialView("_Edit", EducationLevelRepo.byId(id));
        }

        [HttpPost]
        public ActionResult Edit(EducationLevelViewModel model)
        {
            ResponseResult result = EducationLevelRepo.Update(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Delete(int id)
        {
            // id => Category Id
            return PartialView("_Delete", EducationLevelRepo.byId(id));
        }

        [HttpPost]
        public ActionResult Delete(EducationLevelViewModel model)
        {
            ResponseResult result = EducationLevelRepo.Delete(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }
    }
}