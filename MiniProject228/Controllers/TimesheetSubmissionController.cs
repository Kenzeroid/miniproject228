﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using ViewModel;

namespace MiniProject228.Controllers
{
    public class TimesheetSubmissionController : Controller
    {
        // GET: TimesheetSubmission
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List()
        {
            ViewBag.getYear = SubmissionRepo.ListYear();
            ViewBag.getMonth = SubmissionRepo.ListMonth();

            return PartialView("_List");

        }

        public ActionResult EmailCC(List<long> id, List<long> idClient, string year, string month)
        {
            Session["Id"] = id;
            Session["IdClient"] = idClient;
            ViewBag.getYear = SubmissionRepo.ListYear();
            ViewBag.getMonth = SubmissionRepo.ListMonth();

            return PartialView("_EmailCC");
        }

            public ActionResult listTimesheetView(string month, string year)
        {
            ViewBag.getYear = SubmissionRepo.ListYear();
            ViewBag.getMonth = SubmissionRepo.ListMonth();

            List<TimesheetViewModal> listTimesheet = new List<TimesheetViewModal>();
            if ((year != null || year != "") && (month != null || month != ""))
            {
                listTimesheet = SubmissionRepo.getDataTimesheet(year, month);
            }

            ViewBag.idSend = listTimesheet.Select(a => a.id).ToArray();
            ViewBag.idClient = listTimesheet.Select(a => a.idClient).ToArray();

            return View(listTimesheet);
        }

        public ActionResult Send(string kirim, string lewati, string Email) 
        { 
            List<long> id = (List<long>)Session["Id"];
            List<long> idClient = (List<long>)Session["IdClient"];
            ViewBag.getMonth = SubmissionRepo.ListMonth();
            
            string[] result = new string[id.Count];
            string[] resultE = new string[idClient.Count];
            bool value = true;
            List<SelectListItem> emailList = new List<SelectListItem>();

            string j = " ";

            //if (month != null)
            //{
            //    for (int i = 0; i < 12; i++)
            //    {
            //        if (month == ViewBag.getMonth[i].value)
            //        {
            //            j = ViewBag.getMonth[i].text;

            //            break;
            //        }
            //    }
            //}
            //data[1].value = year;
            //data[1].text = year;

            var monthText = j;

            for (int i = 0; i < id.Count(); i++)
            {
                var temp = id[i];
                var tempClient = idClient[i];
                var email = SubmissionRepo.getDataClientById(tempClient);

                result[i] = SubmissionRepo.setSend(temp);
                emailList.Insert(i, new SelectListItem { Text = email.user_email, Value = email.id.ToString() });

                if (result[i] != "ok")
                {
                    value = false;
                }
            }

            //for (int i = 0; i < idClient.Count(); i++)
            //{
            //    var temp = idClient[i];
            //    resultE[i] = SubmissionRepo.sendClient(temp, year, monthText);
            //    if (result[i] != "ok")
            //    {
            //        value = false;
            //    }
            //}
            if (!string.IsNullOrEmpty(kirim) && string.IsNullOrEmpty(lewati))
            {
                MailMessage mail = new MailMessage();
                String[] emailcc = Email.Split(';');
                String subject = "Review Timesheet";
                String body = @"<html>
                      <body>
                      <p>Dear All, </p>
                      <p>Please check timesheet submission to approve or reject</p>
                      <p>Thank you</p> </body> </html>";
                String password = "amiichda";
                SmtpClient client = new SmtpClient();
                mail.From = new MailAddress("hayatiichda35@gmail.com");
                emailList = emailList.OrderByDescending(t => t.Value).ToList();
                String emailUser = emailList[0].Text;
                mail.To.Add(new MailAddress(emailUser));
                for (int x = 1; x < emailList.Count; x++)
                {
                    if (emailUser != emailList[x].Text)
                    {
                        mail.To.Add(new MailAddress(emailList[x].Text));
                    }
                }
                foreach (String mailcc in emailcc)
                {
                    mail.To.Add(new MailAddress(mailcc));
                };
                mail.Subject = subject;
                mail.IsBodyHtml = true;
                mail.Body = body;
                client.Host = "smtp.gmail.com";
                client.Port = 587;
                client.EnableSsl = true;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(mail.From.ToString(), password);
                client.Send(mail);
            }

            if (!string.IsNullOrEmpty(lewati) && string.IsNullOrEmpty(kirim))
            {
                MailMessage mail = new MailMessage();
                String subject = "Review Timesheet";
                String body = @"<html>
                      <body>
                      <p>Dear All, </p>
                      <p>Please check timesheet submission to approve or reject</p>
                      <p>Thank you</p> </body> </html>";
                String password = "amiichda";
                SmtpClient client = new SmtpClient();
                mail.From = new MailAddress("hayatiichda35@gmail.com");
                emailList = emailList.OrderByDescending(t => t.Value).ToList();
                String emailUser = emailList[0].Text;
                mail.To.Add(new MailAddress(emailUser));
                for (int x = 1; x < emailList.Count; x++)
                {
                    if (emailUser != emailList[x].Text)
                    {
                        mail.To.Add(new MailAddress(emailList[x].Text));
                    }
                }
                mail.Subject = subject;
                mail.IsBodyHtml = true;
                mail.Body = body;
                client.Host = "smtp.gmail.com";
                client.Port = 587;
                client.EnableSsl = true;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(mail.From.ToString(), password);
                client.Send(mail);
            }
            if (value == true)
            {
                return Json(new { message = "berhasil" }, JsonRequestBehavior.AllowGet);
                
            }
            else
            {
                return Json(new { message = "gagal" }, JsonRequestBehavior.AllowGet);
            }
            Session["Id"] = null;
            Session["IdClient"] = null;
        }
    }
}