﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewModel;

namespace MiniProject228.Controllers
{
    public class PendidikanController : Controller
    {
        // GET: Keahlian
        public ActionResult Index()
        {
            return View("Index", PendidikanRepo.ByBiodata());
        }
        public ActionResult List(int id)
        {
            Session["Pendidikan"] = id;
            return PartialView("_List", PendidikanRepo.All(id));
        }
        public ActionResult Create()
        {

            ViewBag.EducationLevelList = new SelectList(EducationLevelRepo.All(), "Id", "name");
            return PartialView("_Create", new PendidikanViewModel());
        }

        [HttpPost]
        public ActionResult Create(PendidikanViewModel model)
        {
            if (model.school_name != null && model.education_level_id != null)
            {
                int pend = int.Parse(Session["Pendidikan"].ToString());
                ResponseResult result = PendidikanRepo.Update(model, pend);
                return Json(new
                {
                    success = result.Success,
                    message = result.Message,
                    entity = result.Entity
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new
                {
                    success = false,
                    message = "error",
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Edit(int id)
        {
            // id => Keahlian Id
            PendidikanViewModel model = PendidikanRepo.byId(id);
            ViewBag.EducationLevelList = new SelectList(EducationLevelRepo.All(), "Id", "Name");
            return PartialView("_Edit", model);
        }

        [HttpPost]
        public ActionResult Edit(PendidikanViewModel model)
        {
            if (model.school_name != null && model.education_level_id != null)
            {
                int pend = int.Parse(Session["Pendidikan"].ToString());
                ResponseResult result = PendidikanRepo.Update(model, pend);
                return Json(new
                {
                    success = result.Success,
                    message = result.Message,
                    entity = result.Entity
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new
                {
                    success = false,
                    message = "error",
                }, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult Delete(int id)
        {
            // id => Category Id
            return PartialView("_Delete", PendidikanRepo.byId(id));
        }

        [HttpPost]
        public ActionResult Delete(PendidikanViewModel model)
        {
            ResponseResult result = PendidikanRepo.Delete(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }
    }
}