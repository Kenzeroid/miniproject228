﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewModel;

namespace MiniProject228.Controllers
{
    public class OnlineTestController : Controller
    {
        // GET: OnlineTest
        public ActionResult Index()
        {
            return View("Index", OnlineTestRepo.ByBiodata());
        }
        public ActionResult List(int id)
        {
            Session["Biodata"] = id;
            return PartialView("_List", OnlineTestRepo.All(id));
        }

        public ActionResult Addr(int id)
        {
            return PartialView("_Addr", OnlineTestRepo.ByAddr(id));
        }
        public ActionResult TestType(int Id)
        {
            List<TestDetailViewModel> model = OnlineTestRepo.ById(Id);
            Session["Tipe"] = Id;
            return PartialView("_TestType", model);
        }

        public ActionResult SelectTest(int id)
        {
            TestDetailViewModel type = TestTypeRepo.ByTestType(id);
            return PartialView("_SelectTest", type);
        }

        [HttpPost]
        public ActionResult Simpan(TestViewModel model)
        {
            model.biodata_id = int.Parse(Session["Biodata"].ToString());
            ResultOrder result = OnlineTestRepo.Post(model);
            return Json(
                new
                {
                    success = result.Success,
                    message = result.Message,
                    reference = result.Reference
                }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult Create()
        {

            ViewBag.TestTypeList = new SelectList(TestTypeRepo.All(), "Id", "name");
            return PartialView("_Create", new TestViewModel());
        }

        [HttpPost]
        public ActionResult Create(TestViewModel model)
        {
            int test = int.Parse(Session["Biodata"].ToString());
            ResponseResult result = OnlineTestRepo.Update(model, test);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CreateType()
        {
            ViewBag.TypeList = new SelectList(TestTypeRepo.All(), "Id", "Name");
            return PartialView("_CreateType", new TestDetailViewModel());
        }

        public ActionResult TambahTipe()
        {
            ViewBag.TypeList = new SelectList(TestTypeRepo.All(), "Id", "Name");
            TestDetailViewModel model = new TestDetailViewModel();
            return PartialView("_TambahTipe", model);
        }

        [HttpPost]
        public ActionResult TambahTipe(TestDetailViewModel model)
        {
            model.online_test_id = long.Parse(Session["Tipe"].ToString());
            ResponseResult result = TestTypeRepo.Update(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CreateType(TestDetailViewModel model)
        {
            int test = int.Parse(Session["Biodata"].ToString());
            ResponseResult result = TestTypeRepo.Update(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Edit(int id)
        {
            // id => Keahlian Id
            TestViewModel model = OnlineTestRepo.byId(id);
            ViewBag.TestTypeList = new SelectList(TestTypeRepo.All(), "Id", "Name");
            return PartialView("_Edit", model);
        }

        [HttpPost]
        public ActionResult Edit(TestViewModel model)
        {
            int test = int.Parse(Session["Biodata"].ToString());
            ResponseResult result = OnlineTestRepo.Update(model, test);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Delete(int id)
        {
            // id => Category Id
            return PartialView("_Delete", OnlineTestRepo.byId(id));
        }

        [HttpPost]
        public ActionResult Delete(TestViewModel model)
        {
            ResponseResult result = OnlineTestRepo.Delete(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteTes(int id)
        {
            // id => Category Id
            return PartialView("_DeleteTes", TestTypeRepo.ByDel(id));
        }

        [HttpPost]
        public ActionResult DeleteTes(TestDetailViewModel model)
        {
            ResponseResult result = TestTypeRepo.Delete(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }
    }
}