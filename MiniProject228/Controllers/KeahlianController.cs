﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewModel;

namespace MiniProject228.Controllers
{
    public class KeahlianController : Controller
    {
        // GET: Keahlian
        public ActionResult Index()
        {
            return View("Index", KeahlianRepo.ByBiodata());
        }
        public ActionResult List(int id)
        {
            Session["Biodata"] = id;
            return PartialView("_List", KeahlianRepo.All(id));
        }
        public ActionResult Create()
        {

            ViewBag.SkillLevelList = new SelectList(SkillLevelRepo.All(), "Id", "name");
            return PartialView("_Create", new KeahlianViewModel());
        }

        [HttpPost]
        public ActionResult Create(KeahlianViewModel model)
        {
            int bio = int.Parse(Session["Biodata"].ToString());
            ResponseResult result = KeahlianRepo.Update(model, bio);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Edit(int id)
        {
            // id => Keahlian Id
            KeahlianViewModel model = KeahlianRepo.byId(id);
            ViewBag.SkillLevelList = new SelectList(SkillLevelRepo.All(), "Id", "Name");
            return PartialView("_Edit", model);
        }

        [HttpPost]
        public ActionResult Edit(KeahlianViewModel model)
        {
            int bio = int.Parse(Session["Biodata"].ToString());
            ResponseResult result = KeahlianRepo.Update(model,bio);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Delete(int id)
        {
            // id => Category Id
            return PartialView("_Delete", KeahlianRepo.byId(id));
        }

        [HttpPost]
        public ActionResult Delete(KeahlianViewModel model)
        {
            ResponseResult result = KeahlianRepo.Delete(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }

    }
}