﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewModel;

namespace MiniProject228.Controllers
{
    public class KeluargaController : Controller
    {
        // GET: Keluarga
        public ActionResult Index()
        {
            return View("Index", KeluargaRepo.ByBiodata());
        }
        public ActionResult List(int id)
        {
            Session["Keluarga"] = id;
            return PartialView("_List", KeluargaRepo.All(id));
        }
        public ActionResult Create()
        {

            ViewBag.EducationLevelList = new SelectList(EducationLevelRepo.All(), "id", "name");
            ViewBag.FamilyTypeList = new SelectList(KeluargaRepo.AllFamTree(), "id", "name");
            ViewBag.FamilyRelationList = new SelectList(KeluargaRepo.AllFamRel(), "id", "name");
            return PartialView("_Create", new KeluargaViewModel());
        }

        [HttpPost]
        public ActionResult Create(KeluargaViewModel model)
        {
            if (model.family_tree_type_id != null && model.family_relation_id != null && model.name != null)
            {
                int kel = int.Parse(Session["Keluarga"].ToString());
                ResponseResult result = KeluargaRepo.Update(model, kel);
                return Json(new
                {
                    success = result.Success,
                    message = result.Message,
                    entity = result.Entity
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new
                {
                    success = false,
                    message = "error",
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Edit(int id)
        {
            // id => Keahlian Id
            KeluargaViewModel model = KeluargaRepo.byId(id);
            ViewBag.EducationLevelList = new SelectList(EducationLevelRepo.All(), "id", "name");
            ViewBag.FamilyTypeList = new SelectList(KeluargaRepo.AllFamTree(), "id", "name");
            ViewBag.FamilyRelationList = new SelectList(KeluargaRepo.AllFamRel(), "id", "name");
            return PartialView("_Edit", model);
        }

        [HttpPost]
        public ActionResult Edit(KeluargaViewModel model)
        {
            if (model.family_tree_type_id != null && model.family_relation_id != null && model.name != null)
            {
                int kel = int.Parse(Session["Keluarga"].ToString());
                ResponseResult result = KeluargaRepo.Update(model, kel);
                return Json(new
                {
                    success = result.Success,
                    message = result.Message,
                    entity = result.Entity
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new
                {
                    success = false,
                    message = "error",
                }, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult Delete(int id)
        {
            // id => Category Id
            return PartialView("_Delete", KeluargaRepo.byId(id));
        }

        [HttpPost]
        public ActionResult Delete(KeluargaViewModel model)
        {
            ResponseResult result = KeluargaRepo.Delete(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }
    }
}