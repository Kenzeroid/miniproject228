﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModel;

namespace DataAccess
{
    public class PendidikanRepo
    {
        //Get All

        public static List<PendidikanViewModel> All(int id)
        {
            List<PendidikanViewModel> result = new List<PendidikanViewModel>();
            using (var db = new Context())
            {
                result = (from c in db.x_riwayat_pendidikan
                          join d in db.x_education_level
                          on c.education_level_id equals d.id
                          join e in db.x_biodata
                          on c.biodata_id equals e.id
                          where c.is_delete == false
                          && c.biodata_id == id
                          select new PendidikanViewModel
                          {
                              id = c.id,
                              education_level_id = c.education_level_id,
                              school_name = c.school_name,
                              city = c.city,
                              country = c.country,
                              jenjang_pendidikan = d.name,
                              entry_year = c.entry_year,
                              graduation_year = c.graduation_year,
                              major = c.major,
                              gpa = c.gpa,
                              notes = c.notes
                          }).ToList();
            }
            return result;
        }
        public static List<BiodataViewModel> ByBiodata()
        {
            List<BiodataViewModel> result = new List<BiodataViewModel>();
            using (var db = new Context())
            {
                result = (from e in db.x_biodata
                          select new BiodataViewModel
                          {
                              id = e.id,
                              fullname = e.fullname
                          }).ToList();
            }
            return result;
        }
        public static List<PendidikanViewModel> ByEduLevel(long eduId)
        {
            List<PendidikanViewModel> result = new List<PendidikanViewModel>();
            using (var db = new Context())
            {
                result = (from c in db.x_riwayat_pendidikan
                          join d in db.x_education_level
                          on c.education_level_id equals d.id
                          where c.education_level_id == eduId
                          select new PendidikanViewModel
                          {
                              id = c.id,
                              education_level_id = c.education_level_id,
                              school_name = c.school_name,
                              city = c.city,
                              country = c.country,
                              jenjang_pendidikan = d.name,
                              entry_year = c.entry_year,
                              graduation_year = c.graduation_year,
                              major = c.major,
                              gpa = c.gpa,
                              notes = c.notes
                          }).ToList();
            }
            return result;
        }

        public static PendidikanViewModel byId(int Id)
        {
            //id Pendidikan.Id
            PendidikanViewModel result = new PendidikanViewModel();
            using (var db = new Context())
            {
                result = (from c in db.x_riwayat_pendidikan
                          where c.id == Id
                          select new PendidikanViewModel
                          {
                              id = c.id,
                              education_level_id = c.education_level_id,
                              school_name = c.school_name,
                              city = c.city,
                              country = c.country,
                              entry_year = c.entry_year,
                              graduation_year = c.graduation_year,
                              major = c.major,
                              gpa = c.gpa,
                              notes = c.notes
                          }).FirstOrDefault();
            }
            return result != null ? result : new PendidikanViewModel();
        }
        // Create menu & edit
        public static ResponseResult Update(PendidikanViewModel entity, int pend)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new Context())
                {
                    #region Create New / Insert 
                    if (entity.id == 0)
                    {
                        x_riwayat_pendidikan pendidikan = new x_riwayat_pendidikan();

                        pendidikan.education_level_id = entity.education_level_id;
                        pendidikan.school_name = entity.school_name;
                        pendidikan.city = entity.city;
                        pendidikan.country = entity.country;
                        pendidikan.entry_year = entity.entry_year;
                        pendidikan.graduation_year = entity.graduation_year;
                        pendidikan.major = entity.major;
                        pendidikan.gpa = entity.gpa;
                        pendidikan.notes = entity.notes;
                        pendidikan.biodata_id = pend;

                        pendidikan.created_by = 12345;
                        pendidikan.created_on = DateTime.Now;

                        db.x_riwayat_pendidikan.Add(pendidikan);
                        db.SaveChanges();

                        result.Entity = entity;
                    }

                    #endregion
                    #region Edit

                    else
                    {
                        x_riwayat_pendidikan pendidikan = db.x_riwayat_pendidikan
                        .Where(o => o.id == entity.id)
                        .FirstOrDefault();

                        if (pendidikan != null)
                        {
                            pendidikan.education_level_id = entity.education_level_id;
                            pendidikan.school_name = entity.school_name;
                            pendidikan.city = entity.city;
                            pendidikan.country = entity.country;
                            pendidikan.entry_year = entity.entry_year;
                            pendidikan.graduation_year = entity.graduation_year;
                            pendidikan.major = entity.major;
                            pendidikan.gpa = entity.gpa;
                            pendidikan.notes = entity.notes;

                            pendidikan.modified_by = 12345;
                            pendidikan.modified_on = DateTime.Now;

                            db.SaveChanges();

                            result.Entity = entity;
                        }
                        else
                        {
                            result.Success = false;
                            result.Message = "keahlian tidak ditemukan";
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }
        // Delete
        public static ResponseResult Delete(PendidikanViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new Context())
                {
                    x_riwayat_pendidikan pendidikan = db.x_riwayat_pendidikan
                    .Where(o => o.id == entity.id)
                    .FirstOrDefault();

                    if (pendidikan != null)
                    {
                        pendidikan.is_delete = true;
                        //db.x_keahlian.Remove(keahlian);
                        pendidikan.deleted_by = 12345;
                        pendidikan.deleted_on = DateTime.Now;

                        db.SaveChanges();

                        result.Entity = entity;
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "keahlian tidak ditemukan";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }

    }
}
