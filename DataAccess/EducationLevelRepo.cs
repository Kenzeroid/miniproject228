﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModel;

namespace DataAccess
{
    public class EducationLevelRepo
    {
        //Get All

        public static List<EducationLevelViewModel> All()
        {
            List<EducationLevelViewModel> result = new List<EducationLevelViewModel>();
            using (var db = new Context())
            {
                result = (from c in db.x_education_level
                          select new EducationLevelViewModel
                          {
                              id = c.id,
                              name = c.name,
                              description = c.description
                          }).ToList();
            }
            return result;
        }
        public static EducationLevelViewModel byId(int Id)
        {
            //id EducationLevel.Id
            EducationLevelViewModel result = new EducationLevelViewModel();
            using (var db = new Context())
            {
                result = (from c in db.x_education_level
                          where c.id == Id
                          select new EducationLevelViewModel
                          {
                              id = c.id,
                              name = c.name,
                              description = c.description
                          }).FirstOrDefault();
            }
            return result != null ? result : new EducationLevelViewModel();
        }
        // Create menu & edit
        public static ResponseResult Update(EducationLevelViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new Context())
                {
                    #region Create New / Insert 
                    if (entity.id== 0)
                    {
                        x_education_level edu = new x_education_level();

                        edu.name = entity.name;
                        edu.description = entity.description;

                        edu.created_by = 12345;
                        edu.created_on = DateTime.Now;

                        db.x_education_level.Add(edu);
                        db.SaveChanges();

                        result.Entity = entity;
                    }

                    #endregion
                    #region Edit

                    else
                    {
                        x_education_level edu = db.x_education_level
                        .Where(o => o.id == entity.id)
                        .FirstOrDefault();

                        if (edu != null)
                        {
                            edu.name = entity.name;
                            edu.description = entity.description;

                            edu.created_by = 12345;
                            edu.created_on = DateTime.Now;

                            db.SaveChanges();

                            result.Entity = entity;
                        }
                        else
                        {
                            result.Success = false;
                            result.Message = "skill tidak ditemukan";
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }
        // Delete
        public static ResponseResult Delete(EducationLevelViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new Context())
                {
                    x_education_level edu = db.x_education_level
                    .Where(o => o.id == entity.id)
                    .FirstOrDefault();

                    if (edu != null)
                    {
                        db.x_education_level.Remove(edu);
                        db.SaveChanges();

                        result.Entity = entity;
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "keahlian tidak ditemukan";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }
    }
}
