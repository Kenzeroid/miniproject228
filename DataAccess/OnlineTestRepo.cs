﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using ViewModel;

namespace DataAccess
{
    public class OnlineTestRepo
    {
        //Get All

        public static List<TestViewModel> All(int id)
        {
            List<TestViewModel> result = new List<TestViewModel>();
            using (var db = new Context())
            {
                result = (from c in db.x_online_test
                          join f in db.x_biodata
                          on c.biodata_id equals f.id
                          join a in db.x_addrbook
                          on f.email equals a.email
                          where c.is_delete == false
                          && c.biodata_id == id
                          select new TestViewModel
                          {
                              id = c.id,
                              period_code = c.period_code,
                              period = c.period,
                              test_date = c.test_date,
                              expired_test = c.expired_test,
                              user_access = c.user_access,
                              status = c.status,
                              abuid = a.abuid,
                              abpwd = a.abpwd
                          }).ToList();
            }
            return result;
        }

        public static TestViewModel ByAddr(int id)
        {
            TestViewModel result = new TestViewModel();
            using (var db = new Context())
            {
                result = (from c in db.x_online_test
                          join f in db.x_biodata
                          on c.biodata_id equals f.id
                          join a in db.x_addrbook
                          on f.email equals a.email
                          where c.is_delete == false
                          && c.biodata_id == id
                          select new TestViewModel
                          {
                              id = c.id,
                              period_code = c.period_code,
                              period = c.period,
                              test_date = c.test_date,
                              expired_test = c.expired_test,
                              user_access = c.user_access,
                              status = c.status,
                              abuid = a.abuid,
                              abpwd = a.abpwd
                          }).FirstOrDefault();
            }
            return result;
        }

        public static List<TestDetailViewModel> ById(int id)
        {
            List<TestDetailViewModel> result = new List<TestDetailViewModel>();
            using (var db = new Context())
            {
                result = (from c in db.x_online_test_detail
                          join e in db.x_test_type
                          on c.test_type_id equals e.id
                          join d in db.x_online_test
                          on c.online_test_id equals d.id
                          where c.is_delete == false
                          && c.online_test_id == id
                          select new TestDetailViewModel
                          {
                              id = c.id,
                              online_test_id = d.id,
                              name = e.name
                          }).ToList();
            }
            return result;
        }

        public static List<BiodataViewModel> ByBiodata()
        {
            List<BiodataViewModel> result = new List<BiodataViewModel>();
            using (var db = new Context())
            {
                result = (from e in db.x_biodata
                          select new BiodataViewModel
                          {
                              id = e.id,
                              fullname = e.fullname
                          }).ToList();
            }
            return result;
        }

        public static ResultOrder Post(TestViewModel entity)
        {
            entity.period = generatePeriode(int.Parse(entity.biodata_id.ToString()));
            entity.period_code = generatePeriodeCode(int.Parse(entity.biodata_id.ToString()));
            ResultOrder result = new ResultOrder();
            try
            {
                using (var db = new Context())
                {
                    x_online_test oh = new x_online_test();
                    oh.period_code = entity.period_code;
                    oh.period = entity.period;
                    oh.test_date = entity.test_date;
                    oh.expired_test = entity.expired_test;
                    oh.user_access = entity.user_access;
                    oh.status = entity.status;
                    oh.created_by = 12345;
                    oh.created_on = DateTime.Now;
                    oh.biodata_id = entity.biodata_id;
                    oh.status = "proses";
                    db.x_online_test.Add(oh);
                    db.SaveChanges();
                    foreach (var item in entity.detail)
                    {
                        x_online_test_detail od = new x_online_test_detail();
                        od.online_test_id = oh.id;
                        od.test_type_id = item.test_type_id;
                        od.created_by = 12345;
                        od.created_on = DateTime.Now;
                        db.x_online_test_detail.Add(od);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            SendEmail(entity);
            return result;
        }

        public static void SendEmail(TestViewModel model)
        {
            try
            {
                BiodataViewModel biodataModel = BiodataRepo.getbyId(model.biodata_id);

                var senderEmail = new MailAddress("hayatiichda35@gmail.com", "Ami");
                var receiverEmail = new MailAddress(biodataModel.email, "Receiver");
                var password = "amiichda";
                var subject = "Create New Account";
                var body = "Username:" + biodataModel.abuid + "----" + "password:" + biodataModel.abpwd;
                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(senderEmail.Address, password)
                };
                using (var mess = new MailMessage(senderEmail, receiverEmail)
                {
                    Subject = subject,
                    Body = body
                })
                {
                    smtp.Send(mess);
                }

            }
            catch (Exception)
            {
            }
        }

        public static TestViewModel byId(int Id)
        {
            //id Keahlian.Id
            TestViewModel result = new TestViewModel();
            using (var db = new Context())
            {
                result = (from c in db.x_online_test
                          where c.id == Id
                          select new TestViewModel
                          {
                              id = c.id,
                              period_code = c.period_code,
                              period = c.period,
                              test_date = c.test_date,
                              expired_test = c.expired_test,
                              user_access = c.user_access,
                              status = c.status
                          }).FirstOrDefault();
            }
            return result != null ? result : new TestViewModel();
        }
        // Create menu & edit
        public static ResponseResult Update(TestViewModel entity, int test)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new Context())
                {
                    #region Create New / Insert 
                    if (entity.id == 0)
                    {
                        x_online_test online = new x_online_test();
                        x_biodata biodata = new x_biodata();

                        online.test_date = entity.test_date;
                        online.expired_test = entity.expired_test;
                        online.user_access = entity.user_access;
                        online.status = entity.status;
                        online.biodata_id = test;

                        online.created_by = 12345;
                        online.created_on = DateTime.Now;

                        db.x_online_test.Add(online);
                        db.SaveChanges();

                        //x_online_test_detail type = new x_online_test_detail();

                        //type.online_test_id = online.id;
                        //type.test_type_id = entity.detail.test_type_id;

                        //type.created_by = 12345;
                        //type.created_on = DateTime.Now;

                        //db.x_online_test_detail.Add(type);
                        //db.SaveChanges();

                        result.Entity = entity;
                    }

                    #endregion
                    #region Edit

                    else
                    {
                        x_online_test online = db.x_online_test
                        .Where(o => o.id == entity.id)
                        .FirstOrDefault();

                        if (online != null)
                        {
                            online.period_code = entity.period_code;
                            online.period = entity.period;
                            online.test_date = entity.test_date;
                            online.expired_test = entity.expired_test;
                            online.user_access = entity.user_access;
                            online.status = entity.status;

                            online.modified_by = 12345;
                            online.modified_on = DateTime.Now;

                            db.SaveChanges();

                            result.Entity = entity;
                        }
                        else
                        {
                            result.Success = false;
                            result.Message = "keahlian tidak ditemukan";
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }
        // Delete
        public static ResponseResult Delete(TestViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new Context())
                {
                    x_online_test online = db.x_online_test
                    .Where(o => o.id == entity.id)
                    .FirstOrDefault();

                    if (online != null)
                    {
                        online.is_delete = true;
                        //db.x_keahlian.Remove(keahlian);
                        online.deleted_by = 12345;
                        online.deleted_on = DateTime.Now;

                        db.SaveChanges();

                        result.Entity = entity;
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "keahlian tidak ditemukan";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }

        public static string generatePeriodeCode(int id)
        {
            string Period_Terakhir = "";
            int digit = 4;
            int digitnol = 0;
            string jmlNol = "";
            string strKode = "";
            using (var db = new Context())
            {
                Period_Terakhir = db.x_online_test.Where(a => a.biodata_id == id).OrderByDescending(a => a.period_code).Select(a => a.period_code).FirstOrDefault();
                if (Period_Terakhir == null)
                {
                    Period_Terakhir = "PRD0000";
                }
                strKode = "PRD";

                int angka = int.Parse(Period_Terakhir.Substring(3, 4));
                angka += 1;
                if (angka.ToString().Length <= digit)
                {
                    digitnol = digit - angka.ToString().Length;
                    for (int i = 0; i < digitnol; i++)
                    {
                        jmlNol += "0";
                    }
                }
                Period_Terakhir = strKode + jmlNol + angka;
            }
            return Period_Terakhir;
        }

        public static int generatePeriode(int id)
        {
            int angka = 0;
            string Period_Terakhir = "";
            using (var db = new Context())
            {
                Period_Terakhir = db.x_online_test.Where(a => a.biodata_id == id).OrderByDescending(a => a.period).Select(a => a.period).FirstOrDefault().ToString();

                angka = int.Parse(Period_Terakhir);
                angka += 1;
            }

            return angka;
        }

    }
}
