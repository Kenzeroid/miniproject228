﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModel;

namespace DataAccess
{
    public class TestTypeRepo
    {
        //Get All

        public static List<TestTypeViewModel> All()
        {
            List<TestTypeViewModel> result = new List<TestTypeViewModel>();
            using (var db = new Context())
            {
                result = (from c in db.x_test_type
                          select new TestTypeViewModel
                          {
                              id = c.id,
                              name = c.name,
                              description = c.description
                          }).ToList();
            }
            return result;
        }
        public static List<TestTypeViewModel> byId(int Id)
        {
            //id SkillLevel.Id
            List<TestTypeViewModel> result = new List<TestTypeViewModel>();
            using (var db = new Context())
            {
                result = (from c in db.x_test_type
                          join d in db.x_online_test_detail
                          on c.id equals d.test_type_id
                          join e in db.x_online_test
                          on d.online_test_id equals e.id
                          join f in db.x_biodata
                          on e.biodata_id equals f.id
                          where e.biodata_id == Id
                          select new TestTypeViewModel
                          {
                              id = c.id,
                              name = c.name,
                              description = c.description
                          }).ToList();
            }
            return result;
        }

        public static TestDetailViewModel ByTest(int id)
        {
            //id SkillLevel.Id
            TestDetailViewModel result = new TestDetailViewModel();
            using (var db = new Context())
            {
                result = (from c in db.x_online_test_detail
                          where c.id == id
                          select new TestDetailViewModel
                          {
                              id = c.id,
                              test_type_id = c.test_type_id
                          }).FirstOrDefault();
            }
            return result != null ? result : new TestDetailViewModel();
        }
        public static TestDetailViewModel ByTestType(int id)
        {
            //id SkillLevel.Id
            TestDetailViewModel result = new TestDetailViewModel();
            using (var db = new Context())
            {
                result = (from c in db.x_test_type
                          where c.id == id
                          select new TestDetailViewModel
                          {
                              id = c.id,
                              name = c.name
                          }).FirstOrDefault();
            }
            return result != null ? result : new TestDetailViewModel();
        }

        public static TestDetailViewModel ByDel(int id)
        {
            //id SkillLevel.Id
            TestDetailViewModel result = new TestDetailViewModel();
            using (var db = new Context())
            {
                result = (from c in db.x_online_test_detail
                          where c.id == id
                          select new TestDetailViewModel
                          {
                              id = c.id,
                              online_test_id = c.online_test_id,
                              test_type_id = c.test_type_id
                          }).FirstOrDefault();
            }
            return result != null ? result : new TestDetailViewModel();
        }
        public static TestDetailViewModel ByType(long id)
        {
            TestDetailViewModel result = new TestDetailViewModel();
            using (var db = new Context())
            {
                result = (from a in db.x_online_test_detail
                          join b in db.x_test_type
                          on a.test_type_id equals b.id
                          join c in db.x_online_test
                          on a.online_test_id equals c.id
                          where b.id == id
                          select new TestDetailViewModel
                          {
                              id = a.id,
                              online_test_id = a.online_test_id,
                              test_type_id = a.test_type_id
                          }).FirstOrDefault();
            }
            return result != null ? result : new TestDetailViewModel();
        }


        // Create menu & edit
        public static ResponseResult Update(TestDetailViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new Context())
                {
                    #region Create New / Insert 
                    if (entity.id == 0)
                    {
                        x_online_test_detail type = new x_online_test_detail();

                        type.online_test_id = entity.online_test_id;
                        type.test_type_id = entity.test_type_id;

                        type.created_by = 12345;
                        type.created_on = DateTime.Now;

                        db.x_online_test_detail.Add(type);
                        db.SaveChanges();

                        result.Entity = entity;
                    }

                    #endregion
                    #region Edit

                    else
                    {
                        x_online_test_detail type = db.x_online_test_detail
                        .Where(o => o.id == entity.id)
                        .FirstOrDefault();

                        if (type != null)
                        {
                            type.online_test_id = entity.online_test_id;
                            type.test_type_id = entity.test_type_id;

                            type.created_by = 12345;
                            type.created_on = DateTime.Now;

                            db.SaveChanges();

                            result.Entity = entity;
                        }
                        else
                        {
                            result.Success = false;
                            result.Message = "skill tidak ditemukan";
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }
        // Delete
        public static ResponseResult Delete(TestDetailViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new Context())
                {
                    x_online_test_detail type = db.x_online_test_detail
                    .Where(o => o.id == entity.id)
                    .FirstOrDefault();

                    if (type != null)
                    {
                        type.is_delete = true;
                        type.deleted_by = 12345;
                        type.deleted_on = DateTime.Now;
                        db.SaveChanges();

                        result.Entity = entity;
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "Tipe Tes tidak ditemukan";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }
    }
}
