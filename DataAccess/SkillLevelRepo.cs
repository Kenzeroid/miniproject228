﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModel;

namespace DataAccess
{
    public class SkillLevelRepo
    {
        //Get All

        public static List<SkillViewModel> All()
        {
            List<SkillViewModel> result = new List<SkillViewModel>();
            using (var db = new Context())
            {
                result = (from c in db.x_skill_level
                          select new SkillViewModel
                          {
                              Id = c.id,
                              name = c.name,
                              description = c.description
                          }).ToList();
            }
            return result;
        }
        public static SkillViewModel byId(int Id)
        {
            //id SkillLevel.Id
            SkillViewModel result = new SkillViewModel();
            using (var db = new Context())
            {
                result = (from c in db.x_skill_level
                          where c.id == Id
                          select new SkillViewModel
                          {
                              Id = c.id,
                              name = c.name,
                              description = c.description
                          }).FirstOrDefault();
            }
            return result != null ? result : new SkillViewModel();
        }
        // Create menu & edit
        public static ResponseResult Update(SkillViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new Context())
                {
                    #region Create New / Insert 
                    if (entity.Id == 0)
                    {
                        x_skill_level skill = new x_skill_level();

                        skill.name = entity.name;
                        skill.description = entity.description;

                        skill.created_by = 12345;
                        skill.created_on = DateTime.Now;

                        db.x_skill_level.Add(skill);
                        db.SaveChanges();

                        result.Entity = entity;
                    }

                    #endregion
                    #region Edit

                    else
                    {
                        x_skill_level skill = db.x_skill_level
                        .Where(o => o.id == entity.Id)
                        .FirstOrDefault();

                        if (skill != null)
                        {
                            skill.name = entity.name;
                            skill.description = entity.description;

                            skill.created_by = 12345;
                            skill.created_on = DateTime.Now;

                            db.SaveChanges();

                            result.Entity = entity;
                        }
                        else
                        {
                            result.Success = false;
                            result.Message = "skill tidak ditemukan";
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }
        // Delete
        public static ResponseResult Delete(SkillViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new Context())
                {
                    x_skill_level skill = db.x_skill_level
                    .Where(o => o.id == entity.Id)
                    .FirstOrDefault();

                    if (skill != null)
                    {
                        db.x_skill_level.Remove(skill);
                        db.SaveChanges();

                        result.Entity = entity;
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "keahlian tidak ditemukan";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }
    }
}
