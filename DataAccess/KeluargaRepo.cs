﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModel;

namespace DataAccess
{
    public class KeluargaRepo
    {
        public static List<KeluargaViewModel> All(int id)
        {
            List<KeluargaViewModel> result = new List<KeluargaViewModel>();
            using (var db = new Context())
            {
                result = (from c in db.x_keluarga
                          join d in db.x_family_relation
                          on c.family_relation_id equals d.id
                          join g in db.x_education_level
                          on c.education_level_id equals g.id
                          join e in db.x_biodata
                          on c.biodata_id equals e.id
                          where c.is_delete == false
                          && c.biodata_id == id
                          select new KeluargaViewModel
                          {
                              id = c.id,
                              namaPend = g.name,
                              name = c.name,
                              namaType = d.name,
                              gender = c.gender,
                              dob = c.dob,
                              job = c.job,
                              notes = c.notes
                          }).ToList();
            }
            return result;
        }
        public static List<BiodataViewModel> ByBiodata()
        {
            List<BiodataViewModel> result = new List<BiodataViewModel>();
            using (var db = new Context())
            {
                result = (from e in db.x_biodata
                          select new BiodataViewModel
                          {
                              id = e.id,
                              fullname = e.fullname
                          }).ToList();
            }
            return result;
        }
        public static List<FamilyTreeTypeViewModel> AllFamTree()
        {
            List<FamilyTreeTypeViewModel> result = new List<FamilyTreeTypeViewModel>();
            using (var db = new Context())
            {
                result = (from c in db.x_family_tree_type
                          select new FamilyTreeTypeViewModel
                          {
                              id = c.id,
                              name = c.name,
                              description = c.description
                          }).ToList();
            }
            return result;
        }
        public static List<FamilyRelationViewModel> AllFamRel()
        {
            List<FamilyRelationViewModel> result = new List<FamilyRelationViewModel>();
            using (var db = new Context())
            {
                result = (from c in db.x_family_relation
                          select new FamilyRelationViewModel
                          {
                              id = c.id,
                              name = c.name,
                              description = c.description
                          }).ToList();
            }
            return result;
        }


        public static List<PendidikanViewModel> ByEduLevel(long eduId)
        {
            List<PendidikanViewModel> result = new List<PendidikanViewModel>();
            using (var db = new Context())
            {
                result = (from c in db.x_riwayat_pendidikan
                          join d in db.x_education_level
                          on c.education_level_id equals d.id
                          where c.education_level_id == eduId
                          select new PendidikanViewModel
                          {
                              id = c.id,
                              education_level_id = c.education_level_id,
                              school_name = c.school_name,
                              city = c.city,
                              country = c.country,
                              jenjang_pendidikan = d.name,
                              entry_year = c.entry_year,
                              graduation_year = c.graduation_year,
                              major = c.major,
                              gpa = c.gpa,
                              notes = c.notes
                          }).ToList();
            }
            return result;
        }

        public static KeluargaViewModel byId(int Id)
        {
            //id Pendidikan.Id
            KeluargaViewModel result = new KeluargaViewModel();
            using (var db = new Context())
            {
                result = (from c in db.x_keluarga
                          where c.id == Id
                          select new KeluargaViewModel
                          {
                              id = c.id,
                              education_level_id = c.education_level_id,
                              name = c.name,
                              family_tree_type_id = c.family_tree_type_id,
                              family_relation_id = c.family_relation_id,
                              gender = c.gender,
                              dob = c.dob,
                              job = c.job,
                              notes = c.notes
                          }).FirstOrDefault();
            }
            return result != null ? result : new KeluargaViewModel();
        }
        // Create menu & edit
        public static ResponseResult Update(KeluargaViewModel entity, int kel)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new Context())
                {
                    #region Create New / Insert 
                    if (entity.id == 0)
                    {
                        x_keluarga keluarga = new x_keluarga();

                        keluarga.family_tree_type_id = entity.family_tree_type_id;
                        keluarga.family_relation_id = entity.family_relation_id;
                        keluarga.name = entity.name;
                        keluarga.gender = entity.gender;
                        keluarga.dob = entity.dob;
                        keluarga.education_level_id = entity.education_level_id;
                        keluarga.job = entity.job;
                        keluarga.notes = entity.notes;
                        keluarga.biodata_id = kel;

                        keluarga.created_by = 12345;
                        keluarga.created_on = DateTime.Now;

                        db.x_keluarga.Add(keluarga);
                        db.SaveChanges();

                        result.Entity = entity;
                    }

                    #endregion
                    #region Edit

                    else
                    {
                        x_keluarga keluarga = db.x_keluarga
                        .Where(o => o.id == entity.id)
                        .FirstOrDefault();

                        if (keluarga != null)
                        {
                            keluarga.family_tree_type_id = entity.family_tree_type_id;
                            keluarga.family_relation_id = entity.family_relation_id;
                            keluarga.name = entity.name;
                            keluarga.gender = entity.gender;
                            keluarga.dob = entity.dob;
                            keluarga.education_level_id = entity.education_level_id;
                            keluarga.job = entity.job;
                            keluarga.notes = entity.notes;

                            keluarga.modified_by = 12345;
                            keluarga.modified_on = DateTime.Now;

                            db.SaveChanges();

                            result.Entity = entity;
                        }
                        else
                        {
                            result.Success = false;
                            result.Message = "keluarga tidak ditemukan";
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }
        // Delete
        public static ResponseResult Delete(KeluargaViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new Context())
                {
                    x_keluarga keluarga = db.x_keluarga
                    .Where(o => o.id == entity.id)
                    .FirstOrDefault();

                    if (keluarga != null)
                    {
                        keluarga.is_delete = true;
                        //db.x_keahlian.Remove(keahlian);
                        keluarga.deleted_by = 12345;
                        keluarga.deleted_on = DateTime.Now;

                        db.SaveChanges();

                        result.Entity = entity;
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "keluarga tidak ditemukan";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }
    }
}
