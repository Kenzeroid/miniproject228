﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModel;

namespace DataAccess
{
    public class SubmissionRepo
    {
        public static List<TimesheetViewModal> getDataTimesheet(string year, string month)
        {
            List<TimesheetViewModal> data = new List<TimesheetViewModal>();

            if (year != null || month != null)
            {
                using (var db = new Context())
                {
                    data = (from t in db.x_timesheet
                            join p in db.x_placement
                            on t.placement_id equals p.id
                            join c in db.x_client
                            on p.client_id equals c.id
                            join e in db.x_employee
                            on p.employee_id equals e.id
                            join b in db.x_biodata
                            on e.biodata_id equals b.id
                            where t.is_delete == false && t.ero_status == null && t.user_approval == null
                            select new TimesheetViewModal
                              {
                                  id = t.id,
                                  idClient = p.client_id,
                                  status = t.status,
                                  timesheet_date = t.timesheet_date,
                                  name = c.name,
                                  namaPG = b.nick_name
                              }).ToList();
                }

                data = data.Where(a => a.timesheet_date.ToString().Contains(year)).ToList();

                if (month != "")
                {
                    if (month.Substring(0, 1) == "0")
                    {
                        month = month.Substring(1, 1);
                    }
                    data = data.Where(a => a.timesheet_date.Month.ToString().Contains(month)).ToList();
                }


                return data;

            }

            return data;
        }

        public static List<GeneralViewModel> ListYear()
        {
            long year = (System.DateTime.Now.Year);
            List<GeneralViewModel> alldata = new List<GeneralViewModel>();
            for (long intCount = year; intCount >= 2000; intCount--)
            {
                GeneralViewModel data = new GeneralViewModel();
                data.value = intCount.ToString();
                data.text = intCount.ToString();
                alldata.Add(data);
            }
            return alldata;
        }

        public static List<GeneralViewModel> ListUntilYear()
        {
            long year = (System.DateTime.Now.Year);
            year += 10;
            List<GeneralViewModel> alldata = new List<GeneralViewModel>();
            for (long intCount = year; intCount >= 2000; intCount--)
            {
                GeneralViewModel data = new GeneralViewModel();
                data.value = intCount.ToString();
                data.text = intCount.ToString();
                alldata.Add(data);
            }
            return alldata;
        }

        public static List<GeneralViewModel> ListMonth()
        {
            DateTime month = new DateTime(1999, 1, 1);
            List<GeneralViewModel> alldata = new List<GeneralViewModel>();
            for (long i = 1; i <= 12; i++)
            {
                GeneralViewModel data = new GeneralViewModel();
                CultureInfo ind = new CultureInfo("id-ID");
                string monthname = month.ToString("MMMM", ind);
                if (i < 10)
                {
                    data.value = "0" + i.ToString();
                }
                else
                {
                    data.value = i.ToString();
                }
                data.text = monthname;
                month = month.AddMonths(1);

                alldata.Add(data);
            }
            return alldata;
        }

        public static string setSend(long id)
        {
            x_timesheet data = new x_timesheet();

            using (var db = new Context())
            {
                data = getDataTimesheetById(id);
                data.user_approval = "Submitted";
                data.approved_on = DateTime.Now;

                db.Entry(data).State = EntityState.Modified;
                db.SaveChanges();
            }
            return "ok";
        }
        public static string sendClient(long id, string year, string monthText)
        {
            x_client data = new x_client();

            using (var db = new Context())
            {
                data = getDataClientById(id);

                db.Entry(data).State = EntityState.Modified;
                db.SaveChanges();
            }
            return "ok";
        }
        public static x_timesheet getDataTimesheetById(long id)
        {
            x_timesheet data = new x_timesheet();

            using (var db = new Context())
            {
                data = db.x_timesheet.Where(a => a.is_delete == false && a.id == id).FirstOrDefault();
            }
            return data;
        }

        public static x_client getDataClientById(long id)
        {
            x_client data = new x_client();

            using (var db = new Context())
            {
                data = db.x_client.Where(a => a.is_delete == false && a.id == id).FirstOrDefault();
            }
            return data;
        }
    }
}
