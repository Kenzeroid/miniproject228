﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModel;

namespace DataAccess
{
    public class KeahlianRepo
    {
        //Get All

        public static List<KeahlianViewModel> All(int id)
        {
            List<KeahlianViewModel> result = new List<KeahlianViewModel>();
            using (var db = new Context())
            {
                result = (from c in db.x_keahlian
                          join d in db.x_skill_level
                          on c.skill_level_id equals d.id
                          join e in db.x_biodata
                          on c.biodata_id equals e.id
                          where c.is_delete == false
                          && c.biodata_id == id
                          select new KeahlianViewModel
                          {
                              Id = c.id,
                              skill_name = c.skill_name,
                              level_keahlian = d.name,
                              skill_level_id = c.skill_level_id,
                              notes = c.notes
                          }).ToList();
            }
            return result;
        }
        public static List<BiodataViewModel> ByBiodata()
        {
            List<BiodataViewModel> result = new List<BiodataViewModel>();
            using (var db = new Context())
            {
                result = (from e in db.x_biodata
                          select new BiodataViewModel
                          {
                              id = e.id,
                              fullname = e.fullname
                          }).ToList();
            }
            return result;
        }
        public static List<KeahlianViewModel> BySkillLevel(long skillId)
        {
            List<KeahlianViewModel> result = new List<KeahlianViewModel>();
            using (var db = new Context())
            {
                result = (from c in db.x_keahlian
                          join d in db.x_skill_level
                          on c.skill_level_id equals d.id
                          where c.skill_level_id == skillId
                          select new KeahlianViewModel
                          {
                              Id = c.id,
                              skill_name = c.skill_name,
                              level_keahlian = d.name,
                              skill_level_id = c.skill_level_id,
                              notes = c.notes
                          }).ToList();
            }
            return result;
        }
        public static KeahlianViewModel byId(int Id)
        {
            //id Keahlian.Id
            KeahlianViewModel result = new KeahlianViewModel();
            using (var db = new Context())
            {
                result = (from c in db.x_keahlian
                          where c.id == Id
                          select new KeahlianViewModel
                          {
                              Id = c.id,
                              skill_level_id = c.skill_level_id,
                              skill_name = c.skill_name,
                              notes = c.notes
                          }).FirstOrDefault();
            }
            return result != null ? result : new KeahlianViewModel();
        }
        // Create menu & edit
        public static ResponseResult Update(KeahlianViewModel entity, int bio)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new Context())
                {
                    #region Create New / Insert 
                    if (entity.Id == 0)
                    {
                        x_keahlian keahlian = new x_keahlian();
                        x_biodata biodata = new x_biodata();

                        keahlian.skill_level_id = entity.skill_level_id;
                        keahlian.skill_name = entity.skill_name;
                        keahlian.notes = entity.notes;
                        keahlian.biodata_id = bio;

                        keahlian.created_by = 12345;
                        keahlian.created_on = DateTime.Now;

                        db.x_keahlian.Add(keahlian);
                        db.SaveChanges();

                        result.Entity = entity;
                    }

                    #endregion
                    #region Edit

                    else
                    {
                        x_keahlian keahlian = db.x_keahlian
                        .Where(o => o.id == entity.Id)
                        .FirstOrDefault();

                        if (keahlian != null)
                        {
                            keahlian.skill_level_id = entity.skill_level_id;
                            keahlian.skill_name = entity.skill_name;
                            keahlian.notes = entity.notes;

                            keahlian.modified_by = 12345;
                            keahlian.modified_on = DateTime.Now;

                            db.SaveChanges();

                            result.Entity = entity;
                        }
                        else
                        {
                            result.Success = false;
                            result.Message = "keahlian tidak ditemukan";
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }
        // Delete
        public static ResponseResult Delete(KeahlianViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new Context())
                {
                    x_keahlian keahlian = db.x_keahlian
                    .Where(o => o.id == entity.Id)
                    .FirstOrDefault();

                    if (keahlian != null)
                    {
                        keahlian.is_delete = true;
                        //db.x_keahlian.Remove(keahlian);
                        keahlian.deleted_by = 12345;
                        keahlian.deleted_on = DateTime.Now;

                        db.SaveChanges();

                        result.Entity = entity;
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "keahlian tidak ditemukan";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }

    }
}
