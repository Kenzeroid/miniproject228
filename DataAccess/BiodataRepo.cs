﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using ViewModel;

namespace DataAccess
{
    public class BiodataRepo
    {
        public static BiodataViewModel getbyId(int Id)
        {
            //id Keahlian.Id
            BiodataViewModel result = new BiodataViewModel();
            using (var db = new Context())
            {
                result = (from c in db.x_biodata
                          join d in db.x_addrbook
                          on c.email equals d.email
                          where c.id == Id
                          select new BiodataViewModel
                          {
                              id = c.id,
                              email = c.email,
                              abpwd = d.abpwd,
                              abuid = d.abuid
                          }).FirstOrDefault();
            }
            return result != null ? result : new BiodataViewModel();
        }
    }
}
