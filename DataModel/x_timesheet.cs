namespace DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class x_timesheet
    {
        public long id { get; set; }

        public long created_by { get; set; }

        public DateTime created_on { get; set; }

        public long? modified_by { get; set; }

        public DateTime? modified_on { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_on { get; set; }

        public bool is_delete { get; set; }

        [Required]
        [StringLength(15)]
        public string status { get; set; }

        public long placement_id { get; set; }

        [Column(TypeName = "date")]
        public DateTime timesheet_date { get; set; }

        [Required]
        [StringLength(5)]
        public string start { get; set; }

        [Required]
        [StringLength(5)]
        public string end { get; set; }

        public bool? overtime { get; set; }

        [StringLength(5)]
        public string start_ot { get; set; }

        [StringLength(5)]
        public string end_ot { get; set; }

        [Required]
        [StringLength(255)]
        public string activity { get; set; }

        [StringLength(50)]
        public string user_approval { get; set; }

        public DateTime submitted_on { get; set; }

        public DateTime? approved_on { get; set; }

        [StringLength(50)]
        public string ero_status { get; set; }

        public DateTime? sent_on { get; set; }

        public DateTime? collected_on { get; set; }
    }
}
