﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class KeahlianViewModel
    {
        public long Id { get; set; }

        [StringLength(100)]
        public string skill_name { get; set; }

        public long? skill_level_id { get; set; }

        [StringLength(1000)]
        public string notes { get; set; }
        public string level_keahlian { get; set; }
    }
}
