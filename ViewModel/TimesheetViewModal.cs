﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class TimesheetViewModal
    {
        public long id { get; set; }
        public long idClient { get; set; }

        [Required]
        [StringLength(15)]
        public string status { get; set; }

        public long placement_id { get; set; }

        [Column(TypeName = "date")]
        public DateTime timesheet_date { get; set; }

        public string name { get; set; }
        public string namaPG { get; set; }
        public int tahun { get; set; }
        public int bulan { get; set; }
    }
}
