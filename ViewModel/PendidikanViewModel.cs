﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class PendidikanViewModel
    {
        public long id { get; set; }
        public long? education_level_id { get; set; }

        [Required]
        [StringLength(100)]
        public string school_name { get; set; }

        [Required]
        [StringLength(50)]
        public string city { get; set; }

        [Required]
        [StringLength(50)]
        public string country { get; set; }

        [Required]
        [StringLength(10)]
        public string entry_year { get; set; }

        [Required]
        [StringLength(10)]
        public string graduation_year { get; set; }

        [Required]
        [StringLength(100)]
        public string major { get; set; }

        public decimal? gpa { get; set; }

        [Required]
        [StringLength(1000)]
        public string notes { get; set; }
        public string jenjang_pendidikan { get; set; }
    }
}
