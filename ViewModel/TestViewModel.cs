﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class TestViewModel
    {
        public long id { get; set; }
        public int biodata_id { get; set; }

        [StringLength(50)]
        public string period_code { get; set; }

        public int? period { get; set; }

        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? test_date { get; set; }

        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? expired_test { get; set; }

        [StringLength(10)]
        public string user_access { get; set; }

        [StringLength(50)]
        public string status { get; set; }
        public List<TestDetailViewModel> detail { get; set; }

        [Required]
        [StringLength(50)]
        public string abuid { get; set; }

        [Required]
        [StringLength(50)]
        public string abpwd { get; set; }
    }
}
