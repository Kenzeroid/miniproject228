﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class KeluargaViewModel
    {
        public long id { get; set; }

        public long biodata_id { get; set; }

        public long? family_tree_type_id { get; set; }

        public long? family_relation_id { get; set; }

        [StringLength(100)]
        public string name { get; set; }

        public bool gender { get; set; }

        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? dob { get; set; }

        public long? education_level_id { get; set; }

        [StringLength(100)]
        public string job { get; set; }

        [StringLength(1000)]
        public string notes { get; set; }
        public string namaType { get; set; }
        public string namaPend { get; set; }


    }
}
