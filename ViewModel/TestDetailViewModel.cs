﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class TestDetailViewModel
    {
        public long id { get; set; }

        public long? online_test_id { get; set; }

        public long? test_type_id { get; set; }

        public int? test_order { get; set; }

        [Required]
        [StringLength(50)]
        public string name { get; set; }
    }
}
